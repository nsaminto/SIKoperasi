﻿Imports MySql.Data.MySqlClient
Public Class EntriAngsuran

    Dim angsuran As New ClsAngsuran

    Private Sub EntriAngsuran_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Bersih()
    End Sub

    Private Sub Bersih()
        txt_no_pinj.Enabled = False
        txt_no_angsuran.Enabled = False
        txt_no_angsuran.Text = angsuran.Autonumber
        txt_jml_pinjaman.Enabled = False
        dt_angsuran.Value = Now
        txt_pembayaran.Clear()
        txt_sisa.Clear()
        txt_jml_angsuran.Clear()
        btn_cari_pinjaman.Focus()
    End Sub

    Private Sub SetData()
        angsuran.no_angsuran = txt_no_angsuran.Text
        angsuran.tgl_angsuran = dt_angsuran.Value
        angsuran.pembayaran = txt_pembayaran.Text
        angsuran.sisa_pinjaman = txt_sisa.Text
        angsuran.angsuran_ke = txt_jml_angsuran.Text
    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        angsuran.Simpan()
        Bersih()
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Bersih()
    End Sub

    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub

    Private Sub btn_cari_pinjaman_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari_pinjaman.Click
        popupPinjaman.ShowDialog()
    End Sub
End Class