﻿Imports MySql.Data.MySqlClient
Public Class EntriDataSimpanan
    Dim simpanan As New ClsSimpanan
    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        popupAnggota.ShowDialog()
    End Sub
    Private Sub SetData()
        simpanan.no_simpanan = txt_no_simpanan.Text
        simpanan.tgl_simpanan = dt_simpanan.Value
        simpanan.jns_simpanan = cmb_jenis_simpanan.Text
        simpanan.jml_simpanan = txt_jmlsimpan.Text
        simpanan.jml_tarik = txt_jmltarik.Text
        simpanan.no_anggota = txt_no_anggota.Text
    End Sub
    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        SetData()
        simpanan.Simpan()
        Bersih()
    End Sub
    Private Sub Bersih()
        txt_no_anggota.Enabled = False
        txt_nm_anggota.Enabled = False
        txt_no_simpanan.Enabled = False
        txt_no_simpanan.Text = simpanan.Autonumber
        txt_jmlsimpan.Clear()
        txt_jmltarik.Clear()
        txt_saldolama.Clear()
        cmb_jenis_simpanan.Text = ""
        btn_simpan.Enabled = True
        txt_nm_anggota.Focus()
        txt_saldolama.Text = "0"
    End Sub

    Private Sub EntriDataSimpanan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Bersih()
    End Sub

    Private Sub btn_kuar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_kuar.Click
        Me.Dispose()
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Bersih()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        popupSimpanan.ShowDialog()
    End Sub

    Private Sub txt_jmlsimpan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jmlsimpan.TextChanged

    End Sub

    Private Sub txt_jmltarik_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jmltarik.TextChanged

    End Sub

    Private Sub btn_tarik_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tarik.Click

    End Sub
End Class