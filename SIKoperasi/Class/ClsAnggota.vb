﻿Imports MySql.Data.MySqlClient
Public Class ClsAnggota

    Public no_anggota As String
    Public nm_anggota As String
    Public almt_anggota As String
    Public jenkel As String
    Public status_marital As String
    Public agama As String
    Public pendidikan As String
    Public pekerjaan As String
    Public tlp_anggota As String
    Public jml_keluarga As String

    Public Sub Simpan()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        cmd.CommandText = "insert into anggota (no_anggota, nm_anggota, almt_anggota, jenkel, status_marital, agama, pendidikan, pekerjaan, tlp_anggota, jml_keluarga)" & _
        "Values('" & no_anggota & "','" & nm_anggota & "','" & almt_anggota & "','" & jenkel & "','" & status_marital & "','" & agama & "','" & pendidikan & "','" & pekerjaan & "','" & tlp_anggota & "','" & jml_keluarga & "')"
        cmd.Connection = conn.open
        Dim x As Integer
        x = cmd.ExecuteNonQuery
        If x = 1 Then
            MessageBox.Show("Data tersimpan.", "Pesan")
        Else
            MessageBox.Show("wat")
        End If
        cmd.Connection.Close()
    End Sub


    Public Sub Ubah()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        cmd.CommandText = "update anggota set nm_anggota='" & nm_anggota & "'," & "almt_anggota='" & almt_anggota & "'," & "jenkel='" & jenkel & "'," & "status_marital='" & status_marital & "'," & "agama='" & agama & "'," & "pendidikan='" & pendidikan & "'," & "pekerjaan='" & pekerjaan & "'," & "tlp_anggota='" & tlp_anggota & "'," & "jml_keluarga='" & jml_keluarga & "' where no_anggota='" & no_anggota & "'"
        cmd.Connection = conn.open
        Dim x As Integer
        x = cmd.ExecuteNonQuery
        If x = 1 Then
            MessageBox.Show("Data berhasil diupdate.", "Pesan")
        Else
            MessageBox.Show("wat")
        End If
        cmd.Connection.Close()
    End Sub


    Public Sub Hapus()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        cmd.CommandText = "delete from anggota where no_anggota='" & no_anggota & "'"
        cmd.Connection = conn.open
        Dim x As Integer
        x = cmd.ExecuteNonQuery
        If x = 1 Then
            MessageBox.Show("Data dihapus.", "Pesan")
        Else
            MessageBox.Show("wat")
        End If
        cmd.Connection.Close()
    End Sub


    Public Sub Get_NoAnggota(ByVal kode As String)
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        Dim dreader As MySqlDataReader
        cmd.CommandText = "select * from anggota where no_anggota='" & kode & "'"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        dreader.Read()
        If dreader.HasRows Then
            no_anggota = dreader("no_anggota").ToString
            nm_anggota = dreader("nm_anggota").ToString
            almt_anggota = dreader("almt_anggota").ToString
            jenkel = dreader("jenkel").ToString
            status_marital = dreader("status_marital").ToString
            agama = dreader("agama").ToString
            pendidikan = dreader("pendidikan").ToString
            pekerjaan = dreader("pekerjaan").ToString
            tlp_anggota = dreader("tlp_anggota").ToString
            jml_keluarga = dreader("jml_keluarga").ToString
        Else
            no_anggota = ""
            nm_anggota = ""
            almt_anggota = ""
            jenkel = ""
            status_marital = ""
            agama = ""
            pendidikan = ""
            pekerjaan = ""
            tlp_anggota = ""
            jml_keluarga = ""
        End If
    End Sub

    Public Function Tampil_Anggota()
        Dim conn As New koneksi
        Dim cmd As New MySqlCommand
        Dim sql As String = ""
        Dim dreader As MySqlDataReader
        Dim tmpread As New List(Of ClsAnggota)

        cmd.CommandText = "select * from anggota order by no_anggota"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.HasRows Then
            While dreader.Read
                Dim ftmp As New ClsAnggota

                ftmp.no_anggota = dreader.Item("no_anggota")
                ftmp.nm_anggota = dreader.Item("nm_anggota")
                ftmp.almt_anggota = dreader.Item("almt_anggota")
                ftmp.jenkel = dreader.Item("jenkel")
                ftmp.status_marital = dreader.Item("status_marital")
                ftmp.agama = dreader.Item("agama")
                ftmp.pendidikan = dreader.Item("pendidikan")
                ftmp.pekerjaan = dreader.Item("pekerjaan")
                ftmp.tlp_anggota = dreader.Item("tlp_anggota")
                ftmp.jml_keluarga = dreader.Item("jml_keluarga")
                tmpread.Add(ftmp)
            End While
        End If
        dreader.Close()
        Return tmpread
    End Function

    Public Function Autonumber()
        Dim conn As New koneksi
        Dim dreader As MySqlDataReader
        Dim cmd As New MySqlCommand
        Dim strTemp As String = ""
        Dim konter As String = ""
        Dim kode As String

        cmd.CommandText = "select count(*) as nomor from anggota"
        cmd.Connection = conn.open
        dreader = cmd.ExecuteReader
        If dreader.Read = True Then
            strTemp = dreader.Item("nomor")
            konter = Val(strTemp) + 1
            kode = ("ID" & Mid("00000", 1, 5 - konter.Length) & konter)
        Else
            kode = "ID0001"
            Return (kode)
            Exit Function
        End If
        cmd.Connection.Close()
        Return (kode)
    End Function
End Class
