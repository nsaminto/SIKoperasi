﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EntriPendaftaran
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btn_cari = New System.Windows.Forms.Button
        Me.txt_nm_anggota = New System.Windows.Forms.TextBox
        Me.txt_no_anggota = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.rd2_pri = New System.Windows.Forms.RadioButton
        Me.rd2_sewa = New System.Windows.Forms.RadioButton
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.rd1_pri = New System.Windows.Forms.RadioButton
        Me.rd1_sewa = New System.Windows.Forms.RadioButton
        Me.txt_alamat_usaha = New System.Windows.Forms.TextBox
        Me.txt_penjualan_perbln = New System.Windows.Forms.TextBox
        Me.dt_pendaftaran = New System.Windows.Forms.DateTimePicker
        Me.txt_besar_modal = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.txt_keuntungan_perb = New System.Windows.Forms.TextBox
        Me.txt_bidang_usaha = New System.Windows.Forms.TextBox
        Me.txt_no_pendaftaran = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_cari)
        Me.GroupBox1.Controls.Add(Me.txt_nm_anggota)
        Me.GroupBox1.Controls.Add(Me.txt_no_anggota)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 72)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(435, 100)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Anggota"
        '
        'btn_cari
        '
        Me.btn_cari.Location = New System.Drawing.Point(329, 35)
        Me.btn_cari.Name = "btn_cari"
        Me.btn_cari.Size = New System.Drawing.Size(75, 23)
        Me.btn_cari.TabIndex = 0
        Me.btn_cari.Text = "Cari"
        Me.btn_cari.UseVisualStyleBackColor = True
        '
        'txt_nm_anggota
        '
        Me.txt_nm_anggota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_nm_anggota.Location = New System.Drawing.Point(144, 63)
        Me.txt_nm_anggota.Name = "txt_nm_anggota"
        Me.txt_nm_anggota.Size = New System.Drawing.Size(241, 22)
        Me.txt_nm_anggota.TabIndex = 0
        '
        'txt_no_anggota
        '
        Me.txt_no_anggota.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_anggota.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_anggota.Name = "txt_no_anggota"
        Me.txt_no_anggota.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_anggota.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(45, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Nama"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. Anggota"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(295, 31)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Form Entri Pendaftaran"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Panel4)
        Me.GroupBox3.Controls.Add(Me.Panel3)
        Me.GroupBox3.Controls.Add(Me.txt_alamat_usaha)
        Me.GroupBox3.Controls.Add(Me.txt_penjualan_perbln)
        Me.GroupBox3.Controls.Add(Me.dt_pendaftaran)
        Me.GroupBox3.Controls.Add(Me.txt_besar_modal)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.txt_keuntungan_perb)
        Me.GroupBox3.Controls.Add(Me.txt_bidang_usaha)
        Me.GroupBox3.Controls.Add(Me.txt_no_pendaftaran)
        Me.GroupBox3.Controls.Add(Me.Label19)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Location = New System.Drawing.Point(15, 178)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(435, 392)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Data Pendaftaran"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.rd2_pri)
        Me.Panel4.Controls.Add(Me.rd2_sewa)
        Me.Panel4.Location = New System.Drawing.Point(144, 234)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(241, 25)
        Me.Panel4.TabIndex = 7
        '
        'rd2_pri
        '
        Me.rd2_pri.AutoSize = True
        Me.rd2_pri.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rd2_pri.Location = New System.Drawing.Point(3, 3)
        Me.rd2_pri.Name = "rd2_pri"
        Me.rd2_pri.Size = New System.Drawing.Size(69, 20)
        Me.rd2_pri.TabIndex = 3
        Me.rd2_pri.TabStop = True
        Me.rd2_pri.Text = "Pribadi"
        Me.rd2_pri.UseVisualStyleBackColor = True
        '
        'rd2_sewa
        '
        Me.rd2_sewa.AutoSize = True
        Me.rd2_sewa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rd2_sewa.Location = New System.Drawing.Point(82, 3)
        Me.rd2_sewa.Name = "rd2_sewa"
        Me.rd2_sewa.Size = New System.Drawing.Size(60, 20)
        Me.rd2_sewa.TabIndex = 3
        Me.rd2_sewa.TabStop = True
        Me.rd2_sewa.Text = "Sewa"
        Me.rd2_sewa.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.rd1_pri)
        Me.Panel3.Controls.Add(Me.rd1_sewa)
        Me.Panel3.Location = New System.Drawing.Point(144, 203)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(241, 25)
        Me.Panel3.TabIndex = 6
        '
        'rd1_pri
        '
        Me.rd1_pri.AutoSize = True
        Me.rd1_pri.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rd1_pri.Location = New System.Drawing.Point(3, 3)
        Me.rd1_pri.Name = "rd1_pri"
        Me.rd1_pri.Size = New System.Drawing.Size(69, 20)
        Me.rd1_pri.TabIndex = 3
        Me.rd1_pri.TabStop = True
        Me.rd1_pri.Text = "Pribadi"
        Me.rd1_pri.UseVisualStyleBackColor = True
        '
        'rd1_sewa
        '
        Me.rd1_sewa.AutoSize = True
        Me.rd1_sewa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rd1_sewa.Location = New System.Drawing.Point(82, 3)
        Me.rd1_sewa.Name = "rd1_sewa"
        Me.rd1_sewa.Size = New System.Drawing.Size(60, 20)
        Me.rd1_sewa.TabIndex = 3
        Me.rd1_sewa.TabStop = True
        Me.rd1_sewa.Text = "Sewa"
        Me.rd1_sewa.UseVisualStyleBackColor = True
        '
        'txt_alamat_usaha
        '
        Me.txt_alamat_usaha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_alamat_usaha.Location = New System.Drawing.Point(144, 269)
        Me.txt_alamat_usaha.MaxLength = 45
        Me.txt_alamat_usaha.Multiline = True
        Me.txt_alamat_usaha.Name = "txt_alamat_usaha"
        Me.txt_alamat_usaha.Size = New System.Drawing.Size(241, 114)
        Me.txt_alamat_usaha.TabIndex = 8
        '
        'txt_penjualan_perbln
        '
        Me.txt_penjualan_perbln.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_penjualan_perbln.Location = New System.Drawing.Point(144, 119)
        Me.txt_penjualan_perbln.MaxLength = 9
        Me.txt_penjualan_perbln.Name = "txt_penjualan_perbln"
        Me.txt_penjualan_perbln.Size = New System.Drawing.Size(241, 22)
        Me.txt_penjualan_perbln.TabIndex = 3
        '
        'dt_pendaftaran
        '
        Me.dt_pendaftaran.CustomFormat = "dd/MM/yyyy"
        Me.dt_pendaftaran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_pendaftaran.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_pendaftaran.Location = New System.Drawing.Point(144, 63)
        Me.dt_pendaftaran.Name = "dt_pendaftaran"
        Me.dt_pendaftaran.Size = New System.Drawing.Size(179, 22)
        Me.dt_pendaftaran.TabIndex = 1
        '
        'txt_besar_modal
        '
        Me.txt_besar_modal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_besar_modal.Location = New System.Drawing.Point(144, 175)
        Me.txt_besar_modal.MaxLength = 9
        Me.txt_besar_modal.Name = "txt_besar_modal"
        Me.txt_besar_modal.Size = New System.Drawing.Size(241, 22)
        Me.txt_besar_modal.TabIndex = 5
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(14, 272)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(93, 16)
        Me.Label15.TabIndex = 18
        Me.Label15.Text = "Alamat Usaha"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(13, 239)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(125, 16)
        Me.Label16.TabIndex = 17
        Me.Label16.Text = "Status Tmpt. Usaha"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(13, 208)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(131, 16)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Status Tmpt. Tinggal"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(13, 178)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(85, 16)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "Besar Modal"
        '
        'txt_keuntungan_perb
        '
        Me.txt_keuntungan_perb.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_keuntungan_perb.Location = New System.Drawing.Point(144, 147)
        Me.txt_keuntungan_perb.MaxLength = 9
        Me.txt_keuntungan_perb.Name = "txt_keuntungan_perb"
        Me.txt_keuntungan_perb.Size = New System.Drawing.Size(241, 22)
        Me.txt_keuntungan_perb.TabIndex = 4
        '
        'txt_bidang_usaha
        '
        Me.txt_bidang_usaha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_bidang_usaha.Location = New System.Drawing.Point(144, 91)
        Me.txt_bidang_usaha.MaxLength = 20
        Me.txt_bidang_usaha.Name = "txt_bidang_usaha"
        Me.txt_bidang_usaha.Size = New System.Drawing.Size(241, 22)
        Me.txt_bidang_usaha.TabIndex = 2
        '
        'txt_no_pendaftaran
        '
        Me.txt_no_pendaftaran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_pendaftaran.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_pendaftaran.Name = "txt_no_pendaftaran"
        Me.txt_no_pendaftaran.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_pendaftaran.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(13, 150)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(135, 16)
        Me.Label19.TabIndex = 4
        Me.Label19.Text = "Keuntungan Perbulan"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(13, 122)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(125, 16)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Penjualan Perbulan"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(13, 94)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(94, 16)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "Bidang Usaha"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(13, 66)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(107, 16)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Tgl. Pendaftaran"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 38)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(105, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "No. Pendaftaran"
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(285, 26)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 32)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "Keluar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button2)
        Me.GroupBox4.Controls.Add(Me.Button4)
        Me.GroupBox4.Controls.Add(Me.Button6)
        Me.GroupBox4.Location = New System.Drawing.Point(15, 576)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(435, 75)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(156, 26)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(100, 32)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "Batal"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(20, 26)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(100, 32)
        Me.Button6.TabIndex = 9
        Me.Button6.Text = "Simpan"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'EntriPendaftaran
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(469, 662)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "EntriPendaftaran"
        Me.ShowIcon = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "EntriPendaftaran"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_cari As System.Windows.Forms.Button
    Friend WithEvents txt_nm_anggota As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_anggota As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_besar_modal As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_keuntungan_perb As System.Windows.Forms.TextBox
    Friend WithEvents txt_bidang_usaha As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_pendaftaran As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents rd2_pri As System.Windows.Forms.RadioButton
    Friend WithEvents rd2_sewa As System.Windows.Forms.RadioButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents rd1_pri As System.Windows.Forms.RadioButton
    Friend WithEvents rd1_sewa As System.Windows.Forms.RadioButton
    Friend WithEvents txt_alamat_usaha As System.Windows.Forms.TextBox
    Friend WithEvents txt_penjualan_perbln As System.Windows.Forms.TextBox
    Friend WithEvents dt_pendaftaran As System.Windows.Forms.DateTimePicker
End Class
