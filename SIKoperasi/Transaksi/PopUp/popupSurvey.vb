﻿Imports MySql.Data.MySqlClient
Public Class popupSurvey
    Dim survey As New ClsSurvey
    Private Sub popupSurvey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Me.tabel_survey()
    End Sub
    Private Sub tabel_survey()
        ListView1.Clear()
        Me.ListView1.View = View.Details
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(0).Text = "No."
        Me.ListView1.Columns(0).Width = 30
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(1).Text = "No. Survey"
        Me.ListView1.Columns(1).Width = 80
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(2).Text = "Tgl. Survey"
        Me.ListView1.Columns(2).Width = 70

        Dim objList As List(Of ClsSurvey)
        objList = survey.Tampil_Survey

        ListView1.Items.Clear()
        For i As Integer = 0 To objList.Count - 1
            ListView1.Items.Add(i + 1)
            ListView1.Items(i).SubItems.Add(objList.Item(i).no_survey)
            ListView1.Items(i).SubItems.Add(objList.Item(i).tgl_survey)
        Next
    End Sub

    Private Sub ListView1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView1.MouseClick
        CetakSPPD.txt_no_survey.Text = ListView1.SelectedItems(0).SubItems(1).Text
        CetakSPPD.dt_survey.Value = ListView1.SelectedItems(0).SubItems(2).Text
        Me.Dispose()
    End Sub
End Class