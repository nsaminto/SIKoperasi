﻿Imports MySql.Data.MySqlClient
Public Class popupPinjaman
    Dim pinjaman As New ClsPinjaman
    Dim anggota As New ClsAnggota
    Dim pendaftaran As New ClsPendaftaran
    Dim frmname As String
    Private Sub popupPinjaman_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Me.tabel_pinjaman()
        Me.frmname = Form.ActiveForm.Name
    End Sub

    Private Sub tabel_pinjaman()
        ListView1.Clear()
        Me.ListView1.View = View.Details
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(0).Text = "No."
        Me.ListView1.Columns(0).Width = 30
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(1).Text = "No. Pinjaman"
        Me.ListView1.Columns(1).Width = 80
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(2).Text = "Tgl. Pinjaman"
        Me.ListView1.Columns(2).Width = 70
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(3).Text = "Jml. Pinjaman"
        Me.ListView1.Columns(3).Width = 80
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(4).Text = "ID Pinjaman"
        Me.ListView1.Columns(4).Width = 30

        Dim objList As List(Of ClsPinjaman)
        objList = pinjaman.Tampil_Pinjaman

        ListView1.Items.Clear()
        For i As Integer = 0 To objList.Count - 1
            ListView1.Items.Add(i + 1)
            ListView1.Items(i).SubItems.Add(objList.Item(i).no_pinjaman)
            ListView1.Items(i).SubItems.Add(objList.Item(i).tgl_pinjaman)
            ListView1.Items(i).SubItems.Add(objList.Item(i).jml_pinjaman)
            ListView1.Items(i).SubItems.Add(objList.Item(i).no_anggota)
        Next
    End Sub

    Private Sub ListView1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView1.MouseDoubleClick
        If frmname = "CetakSurvey" Then
            CetakSurvey.txt_no_pinj.Text = ListView1.SelectedItems(0).SubItems(1).Text
            anggota.Get_NoAnggota(ListView1.SelectedItems(0).SubItems(4).Text)
            CetakSurvey.txt_nama.Text = anggota.nm_anggota
            CetakSurvey.txt_jml.Text = ListView1.SelectedItems(0).SubItems(3).Text
            pendaftaran.Get_NoPendaftaran(ListView1.SelectedItems(0).SubItems(4).Text)
            CetakSurvey.txt_bidang.Text = pendaftaran.bidang_usaha
            Me.Dispose()
        ElseIf frmname = "EntriAngsuran" Then
            EntriAngsuran.txt_no_pinj.Text = ListView1.SelectedItems(0).SubItems(1).Text
            EntriAngsuran.txt_jml_pinjaman.Text = ListView1.SelectedItems(0).SubItems(3).Text
            Me.Dispose()
        End If

    End Sub
End Class