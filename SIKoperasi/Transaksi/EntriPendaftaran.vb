﻿Imports MySql.Data.MySqlClient
Public Class EntriPendaftaran
    Dim pendaftaran As New ClsPendaftaran
    Private Sub bersih()
        txt_no_anggota.Enabled = False
        txt_nm_anggota.Enabled = False
        txt_no_pendaftaran.Enabled = False
        txt_no_pendaftaran.Text = pendaftaran.Autonumber
        dt_pendaftaran.Value = Now
        txt_bidang_usaha.Clear()
        txt_penjualan_perbln.Clear()
        txt_keuntungan_perb.Clear()
        txt_besar_modal.Clear()
        txt_alamat_usaha.Clear()
        rd1_pri.Checked = True
        rd2_pri.Checked = True
        btn_cari.Focus()
    End Sub
    Private Sub SetData()
        pendaftaran.no_anggota = txt_no_anggota.Text
        pendaftaran.no_pendaftaran = txt_no_pendaftaran.Text
        pendaftaran.tgl_pendaftaran = dt_pendaftaran.Value
        pendaftaran.bidang_usaha = txt_bidang_usaha.Text
        pendaftaran.penjualan_perbulan = txt_penjualan_perbln.Text
        pendaftaran.keuntungan_perbulan = txt_keuntungan_perb.Text
        pendaftaran.besar_modal = txt_besar_modal.Text
        If rd1_pri.Checked = True Then
            pendaftaran.tempat_tinggal = 0
        ElseIf rd1_sewa.Checked = True Then
            pendaftaran.tempat_tinggal = 1
        Else
            MessageBox.Show("wat")
        End If
        If rd2_pri.Checked = True Then
            pendaftaran.tempat_usaha = 0
        ElseIf rd2_sewa.Checked = True Then
            pendaftaran.tempat_usaha = 1
        Else
            MessageBox.Show("wat2")
        End If
        pendaftaran.almt_usaha = txt_alamat_usaha.Text
    End Sub
    Private Sub EntriPendaftaran_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bersih()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Dispose()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        bersih()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        SetData()
        pendaftaran.Simpan()
        bersih()
    End Sub

    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        popupAnggota.ShowDialog()
    End Sub
End Class