﻿Imports MySql.Data.MySqlClient
Public Class popupAnggota
    Dim anggota As New ClsAnggota
    Dim frmname As String
    Private Sub tabel_anggota()
        ListView1.Clear()
        Me.ListView1.View = View.Details
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(0).Text = "No."
        Me.ListView1.Columns(0).Width = 30
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(1).Text = "No. Anggota"
        Me.ListView1.Columns(1).Width = 80
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(2).Text = "Nama"
        Me.ListView1.Columns(2).Width = 120
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(3).Text = "Alamat"
        Me.ListView1.Columns(3).Width = 120
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(4).Text = "Jenkel"
        Me.ListView1.Columns(4).Width = 30
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(5).Text = "Status"
        Me.ListView1.Columns(5).Width = 30
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(6).Text = "Agama"
        Me.ListView1.Columns(6).Width = 60
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(7).Text = "Pendidikan"
        Me.ListView1.Columns(7).Width = 60
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(8).Text = "Pekerjaan"
        Me.ListView1.Columns(8).Width = 60
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(9).Text = "Telepon"
        Me.ListView1.Columns(9).Width = 50
        Me.ListView1.Columns.Add(New ColumnHeader)
        Me.ListView1.Columns(10).Text = "Jumlah Kel."
        Me.ListView1.Columns(10).Width = 30

        Dim objList As List(Of ClsAnggota)
        objList = anggota.Tampil_Anggota

        ListView1.Items.Clear()
        For i As Integer = 0 To objList.Count - 1
            ListView1.Items.Add(i + 1)
            ListView1.Items(i).SubItems.Add(objList.Item(i).no_anggota)
            ListView1.Items(i).SubItems.Add(objList.Item(i).nm_anggota)
            ListView1.Items(i).SubItems.Add(objList.Item(i).almt_anggota)
            ListView1.Items(i).SubItems.Add(objList.Item(i).jenkel)
            ListView1.Items(i).SubItems.Add(objList.Item(i).status_marital)
            ListView1.Items(i).SubItems.Add(objList.Item(i).agama)
            ListView1.Items(i).SubItems.Add(objList.Item(i).pendidikan)
            ListView1.Items(i).SubItems.Add(objList.Item(i).pekerjaan)
            ListView1.Items(i).SubItems.Add(objList.Item(i).tlp_anggota)
            ListView1.Items(i).SubItems.Add(objList.Item(i).jml_keluarga)
        Next
    End Sub
 
    Private Sub popupAnggota_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.frmname = Form.ActiveForm.Name
        Call Me.tabel_anggota()

    End Sub

    Private Sub ListView1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView1.MouseClick
        If frmname = "EntriDataAnggota" Then
            EntriDataAnggota.txt_no_anggota.Text = ListView1.SelectedItems(0).SubItems(1).Text
            anggota.Get_NoAnggota(EntriDataAnggota.txt_no_anggota.Text)
            EntriDataAnggota.txt_nm_anggota.Text = anggota.nm_anggota
            EntriDataAnggota.txt_almt_anggota.Text = anggota.almt_anggota

            ' Check Radio button for jenkel
            If anggota.jenkel = "0" Then
                EntriDataAnggota.rd1_pria.Checked = True
            ElseIf anggota.jenkel = "1" Then
                EntriDataAnggota.rd1_wanita.Checked = True
            End If

            ' Check Radio Button for status_marital
            If anggota.status_marital = "0" Then
                EntriDataAnggota.rd2_nikah.Checked = True
            ElseIf anggota.status_marital = "1" Then
                EntriDataAnggota.rd2_single.Checked = True
            ElseIf anggota.status_marital = "2" Then
                EntriDataAnggota.rd2_cerai.Checked = True
            End If

            EntriDataAnggota.cmb_agama.Text = anggota.agama
            EntriDataAnggota.txt_pekerjaan.Text = anggota.pekerjaan
            EntriDataAnggota.txt_pendidikan.Text = anggota.pendidikan
            EntriDataAnggota.txt_tlp.Text = anggota.tlp_anggota
            EntriDataAnggota.txt_jml_keluarga.Text = anggota.jml_keluarga
            EntriDataAnggota.txt_nm_anggota.Focus()
            EntriDataAnggota.btn_simpan.Enabled = False
            EntriDataAnggota.btn_ubah.Enabled = True
            EntriDataAnggota.btn_hapus.Enabled = True
            Me.Dispose()
        ElseIf frmname = "EntriDataSimpanan" Then
            EntriDataSimpanan.txt_no_anggota.Text = ListView1.SelectedItems(0).SubItems(1).Text
            anggota.Get_NoAnggota(EntriDataSimpanan.txt_no_anggota.Text)
            EntriDataSimpanan.txt_nm_anggota.Text = anggota.nm_anggota
            EntriDataSimpanan.cmb_jenis_simpanan.Focus()
            Me.Dispose()
        ElseIf frmname = "EntriPengunduranDiri" Then
            EntriPengunduranDiri.txt_no_anggota.Text = ListView1.SelectedItems(0).SubItems(1).Text
            anggota.Get_NoAnggota(EntriPengunduranDiri.txt_no_anggota.Text)
            EntriPengunduranDiri.txt_nm_anggota.Text = anggota.nm_anggota
            EntriPengunduranDiri.txt_alasan.Focus()
            Me.Dispose()
        ElseIf frmname = "EntriPendaftaran" Then
            EntriPendaftaran.txt_no_anggota.Text = ListView1.SelectedItems(0).SubItems(1).Text
            anggota.Get_NoAnggota(EntriPendaftaran.txt_no_anggota.Text)
            EntriPendaftaran.txt_nm_anggota.Text = anggota.nm_anggota
            EntriPendaftaran.txt_besar_modal.Focus()
            Me.Dispose()
        ElseIf frmname = "EntriPinjaman" Then
            EntriPinjaman.txt_no_anggota.Text = ListView1.SelectedItems(0).SubItems(1).Text
            anggota.Get_NoAnggota(EntriPinjaman.txt_no_anggota.Text)
            EntriPinjaman.txt_nm_anggota.Text = anggota.nm_anggota
            EntriPinjaman.txt_jmlsimpan.Focus()
            Me.Dispose()
        End If
    End Sub

End Class