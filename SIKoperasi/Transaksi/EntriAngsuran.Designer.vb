﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EntriAngsuran
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txt_pembayaran = New System.Windows.Forms.TextBox
        Me.txt_jml_angsuran = New System.Windows.Forms.TextBox
        Me.dt_angsuran = New System.Windows.Forms.DateTimePicker
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txt_sisa = New System.Windows.Forms.TextBox
        Me.txt_no_angsuran = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btn_cari_pinjaman = New System.Windows.Forms.Button
        Me.txt_jml_pinjaman = New System.Windows.Forms.TextBox
        Me.txt_no_pinj = New System.Windows.Forms.TextBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btn_kuar = New System.Windows.Forms.Button
        Me.btn_batal = New System.Windows.Forms.Button
        Me.btn_simpan = New System.Windows.Forms.Button
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txt_pembayaran)
        Me.GroupBox2.Controls.Add(Me.txt_jml_angsuran)
        Me.GroupBox2.Controls.Add(Me.dt_angsuran)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.txt_sisa)
        Me.GroupBox2.Controls.Add(Me.txt_no_angsuran)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 239)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(435, 219)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Angsuran"
        '
        'txt_pembayaran
        '
        Me.txt_pembayaran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_pembayaran.Location = New System.Drawing.Point(144, 94)
        Me.txt_pembayaran.MaxLength = 9
        Me.txt_pembayaran.Name = "txt_pembayaran"
        Me.txt_pembayaran.Size = New System.Drawing.Size(179, 22)
        Me.txt_pembayaran.TabIndex = 17
        '
        'txt_jml_angsuran
        '
        Me.txt_jml_angsuran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jml_angsuran.Location = New System.Drawing.Point(144, 150)
        Me.txt_jml_angsuran.MaxLength = 2
        Me.txt_jml_angsuran.Name = "txt_jml_angsuran"
        Me.txt_jml_angsuran.Size = New System.Drawing.Size(59, 22)
        Me.txt_jml_angsuran.TabIndex = 5
        '
        'dt_angsuran
        '
        Me.dt_angsuran.CustomFormat = "dd/MM/yyyy"
        Me.dt_angsuran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dt_angsuran.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dt_angsuran.Location = New System.Drawing.Point(144, 64)
        Me.dt_angsuran.Name = "dt_angsuran"
        Me.dt_angsuran.Size = New System.Drawing.Size(179, 22)
        Me.dt_angsuran.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(13, 153)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(83, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Angsuran ke"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(13, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(94, 16)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Sisa Pinjaman"
        '
        'txt_sisa
        '
        Me.txt_sisa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_sisa.Location = New System.Drawing.Point(144, 122)
        Me.txt_sisa.MaxLength = 9
        Me.txt_sisa.Name = "txt_sisa"
        Me.txt_sisa.Size = New System.Drawing.Size(179, 22)
        Me.txt_sisa.TabIndex = 4
        '
        'txt_no_angsuran
        '
        Me.txt_no_angsuran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_angsuran.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_angsuran.Name = "txt_no_angsuran"
        Me.txt_no_angsuran.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_angsuran.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 95)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Pembayaran"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(13, 69)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 16)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Tgl. Angsuran"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(13, 38)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(89, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "No. Angsuran"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_cari_pinjaman)
        Me.GroupBox1.Controls.Add(Me.txt_jml_pinjaman)
        Me.GroupBox1.Controls.Add(Me.txt_no_pinj)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 122)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(435, 111)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Pinjaman"
        '
        'btn_cari_pinjaman
        '
        Me.btn_cari_pinjaman.Location = New System.Drawing.Point(329, 35)
        Me.btn_cari_pinjaman.Name = "btn_cari_pinjaman"
        Me.btn_cari_pinjaman.Size = New System.Drawing.Size(75, 23)
        Me.btn_cari_pinjaman.TabIndex = 0
        Me.btn_cari_pinjaman.Text = "Cari"
        Me.btn_cari_pinjaman.UseVisualStyleBackColor = True
        '
        'txt_jml_pinjaman
        '
        Me.txt_jml_pinjaman.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_jml_pinjaman.Location = New System.Drawing.Point(144, 63)
        Me.txt_jml_pinjaman.Name = "txt_jml_pinjaman"
        Me.txt_jml_pinjaman.Size = New System.Drawing.Size(241, 22)
        Me.txt_jml_pinjaman.TabIndex = 1
        '
        'txt_no_pinj
        '
        Me.txt_no_pinj.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_no_pinj.Location = New System.Drawing.Point(144, 35)
        Me.txt_no_pinj.Name = "txt_no_pinj"
        Me.txt_no_pinj.Size = New System.Drawing.Size(179, 22)
        Me.txt_no_pinj.TabIndex = 0
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(13, 66)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(110, 16)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Jumlah Pinjaman"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 38)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(88, 16)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "No. Pinjaman"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(22, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(193, 31)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Entri Angsuran"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btn_kuar)
        Me.GroupBox3.Controls.Add(Me.btn_batal)
        Me.GroupBox3.Controls.Add(Me.btn_simpan)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 464)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(435, 75)
        Me.GroupBox3.TabIndex = 27
        Me.GroupBox3.TabStop = False
        '
        'btn_kuar
        '
        Me.btn_kuar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_kuar.Location = New System.Drawing.Point(315, 26)
        Me.btn_kuar.Name = "btn_kuar"
        Me.btn_kuar.Size = New System.Drawing.Size(75, 32)
        Me.btn_kuar.TabIndex = 11
        Me.btn_kuar.Text = "Keluar"
        Me.btn_kuar.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Location = New System.Drawing.Point(186, 26)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(78, 32)
        Me.btn_batal.TabIndex = 10
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan
        '
        Me.btn_simpan.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan.Location = New System.Drawing.Point(50, 26)
        Me.btn_simpan.Name = "btn_simpan"
        Me.btn_simpan.Size = New System.Drawing.Size(90, 32)
        Me.btn_simpan.TabIndex = 7
        Me.btn_simpan.Text = "Simpan"
        Me.btn_simpan.UseVisualStyleBackColor = True
        '
        'EntriAngsuran
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 639)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "EntriAngsuran"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Entri Angsuran"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txt_jml_angsuran As System.Windows.Forms.TextBox
    Friend WithEvents dt_angsuran As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_sisa As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_angsuran As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_pembayaran As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_cari_pinjaman As System.Windows.Forms.Button
    Friend WithEvents txt_jml_pinjaman As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_pinj As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_kuar As System.Windows.Forms.Button
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents btn_simpan As System.Windows.Forms.Button
End Class
